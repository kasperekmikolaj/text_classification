from pre_processing.texts_info import categories


labels_set = "{Albania,Amerykanscy-prozaicy,Arabowie,Astronautyka,Choroby,Egipt,Ekologia-roslin,Filmy-animowane,Galezie-prawa,Gry-komputerowe,Karkonosze,Katolicyzm,Komiksy,Komputery,Kotowate,Kultura-Chin,Monety,Muzyka-powazna,Narciarstwo,Narkomania,Niemieccy-wojskowi,Optyka,Pierwiastki-chemiczne,Pilka-nozna,Propaganda-polityczna,Rachunkowosc,Samochody,Samoloty,Sporty-silowe,System-opieki-zdrowotnej-w-Polsce,Szachy,Wojska-pancerne,Zegluga,Zydzi}"


if __name__ == '__main__':
    file_path = "generated_arff.arff"
    f = open(file_path, 'w', encoding='utf8')
    f.write("@RELATION si4\n\n")
    f.write("@ATTRIBUTE article_text string\n")
    f.write(f"@ATTRIBUTE text_label {labels_set}\n\n")
    f.write("@DATA\n")

    for text_cat in categories:
        for i in range(1, text_cat[1] + 1):
            text_label = text_cat[0]
            file_path = "../prepared_texts/" + text_cat[0] + "/" + text_cat[0] + "_" + str(i) + ".txt"
            file = open(file_path, 'r', encoding='utf8')
            article_text = file.read()
            f.write("\"" + article_text + '\",' + text_label + '\n')
            file.close()
    f.close()




