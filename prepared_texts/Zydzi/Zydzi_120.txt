adolph zukor ur stycznia zm czerwca amerykański producent filmowy założyciel paramount pictures urodził się w ricse węgry w żydowskiej rodzinie w wieku lat wyemigrował do stanów zjednoczonych jak większość emigrantów zaczął skromnie najpierw przybył do nowego jorku pozostał przy rodzinie i pracował w sklepie tapicerskim przyjaciel załatwił mu pracę praktykanta u kuśnierza zukor pozostał tam przez dwa lata kiedy odszedł by stać się pracownikiem kontraktowym zszywał kawałki futra i sprzedawał sam miał wówczas dziewiętnaście lat i znakomitego projektanta ale był młody i odważny w wystawa columbian exposition w chicago czcząca odkrycie ameryki przez kolumba przyciągnęła zukora na środkowy zachód zaczął tam futrzany biznes w drugim sezonie działalności zukors novelty fur company rozrosło się do dwudziestu pięciu ludzi i otworzyło filię jednym z mitów historii filmu jest że ludzie którzy tworzyli przemysł filmowy byli ubogimi młodymi prostakami zukor wyraźnie nie pasował do tego profilu około wyglądał już i żył jak bogaty młody mieszczanin miał apartament na ulicy i siódmej alei w nowym jorku w bogatej niemieckożydowskiej dzielnicy włączył się do przemysłu filmowego kiedy w jego kuzyn max goldstein zwrócił się do niego z prośbą o pożyczkę mitchell mark potrzebował inwestorów by rozszerzyć swą sieć teatrów podobnych do tego w buffalo w stanie nowy jork z edisonia hall salon arkady miał prezentować cuda thomasa edisona gramofony elektryczne światła i kino zukor nie tylko dał goldsteinowi pieniądze ale nalegał na utworzenie spółki innym partnerem w przedsięwzięciu był marcus loew w zukor założył famous players in famous plays jako amerykańskie przedsiębiorstwo dystrybucji francuskiego filmu les amours de reine la élisabeth z sarą bernhardt w następnym roku otrzymał finansowe poparcie braci frohman potężnych menadżerów teatralnych z nowego jorku ich najważniejszym zadaniem było przyciągnięcie znanych aktorów sceny do kina w tym celu utworzyli famous players film company które wyprodukowało więźnia zendy studio przekształciło się w famous playerslasky a następnie paramount pictures zukor był prezydentem paramountu do gdy został prezesem zarządu zrewolucjonizował przemysł filmu przez zorganizowanie produkcji dystrybucji w jednym przedsiębiorstwie zukor był znakomitym dyrektorem i producentem odszedł z paramount pictures w od tego czasu pełnił funkcję honorowego przewodniczącego to stanowisko utrzymał do śmierci w wieku lat w los angeles