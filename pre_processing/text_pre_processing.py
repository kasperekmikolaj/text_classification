import errno
import os
from typing import List

from pre_processing.texts_info import categories


def prepare_file_and_save(file_path: str, path_to_save: str) -> None:
    file = open(file_path, mode='r', encoding='utf8')
    text: str = file.read().lower().replace("\n", ' ')
    t = list(filter(check_character, text))
    t = list_to_string(t)
    result = ' '.join(t.split())
    if not os.path.exists(os.path.dirname(path_to_save)):
        try:
            os.makedirs(os.path.dirname(path_to_save))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    new_file = open(path_to_save, mode='w', encoding='utf8')
    new_file.write(result)
    file.close()
    new_file.close()


def check_character(char: str) -> bool:
    if ord(char) == 32 or char.isalpha():
        return True
    else:
        return False


def list_to_string(list_of_strings: List[str]) -> str:
    res = ""
    for ele in list_of_strings:
        res += ele
    return res


if __name__ == '__main__':
    for text_cat in categories:
        for i in range(1, text_cat[1] + 1):
            file_path = "../texts/" + text_cat[0] + "/" + text_cat[0] + "_" + str(i) + ".txt"
            path_to_save = "../prepared_texts/" + text_cat[0] + "/" + text_cat[0] + "_" + str(i) + ".txt"
            prepare_file_and_save(file_path, path_to_save)